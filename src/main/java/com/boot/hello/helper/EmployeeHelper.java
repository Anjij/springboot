package com.boot.hello.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.boot.hello.employee.Employee;
import com.boot.hello.service.EmployeeService;
import com.boot.hello.util.APIResponse;
import com.boot.hello.util.Status;
//import com.sp.test.helper.EmployeeNotFoundException;

@Configuration
@Service
public class EmployeeHelper {
	
	@Autowired
	private EmployeeService employeeService;
	
	public APIResponse<Employee> getEmployee(Integer id) {
		APIResponse<Employee> apiResponse = new APIResponse<>();
		Employee employee =  employeeService.getEmployee(id);
		if(employee == null)
			throw new EmployeeNotFoundException("employee id "+id+" not found");
		
		apiResponse.setPayLoad(employee);
		apiResponse.setStatus(new Status());
		return apiResponse;
	}

	
	  public APIResponse<Employee> insertEmployee(Employee employee) {
		 APIResponse<Employee> apiresponse=new APIResponse<>();
	  Employee employee1 = employeeService.insertEmployee(employee);
	  
	  return apiresponse;
	  
	  }
	  public APIResponse<Employee> updateEmployee(Employee employee) {
			 APIResponse<Employee> apiResponse=new APIResponse<>();
			 Employee newemployee=employeeService.updateEmployee(employee);
			 if(newemployee==null)
		//	 throw new EmployeeNotFoundException("444","Employee updation failed");
			 apiResponse.setPayLoad(employee);
			 apiResponse.setStatus(new Status());
			return apiResponse;

		}

		public APIResponse<Employee> deleteEmployee(Integer id) {
			APIResponse<Employee> apiResponse=new APIResponse<>();
			 String newemployee=employeeService.deleteEmployee(id);
			 if(newemployee==null)
			//	 throw new EmployeeNotFoundException("555","Employee with "+ id+" delete failed");
			 apiResponse.setPayLoad(null);
			 apiResponse.setStatus(new Status());
			return apiResponse;

		}
	 }
