package com.boot.hello.helper;

public class EmployeeNotFoundException extends RuntimeException {

	public EmployeeNotFoundException(String arg0) {
		super (arg0);
	}
}
