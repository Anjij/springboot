package com.boot.hello.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boot.hello.employee.Employee;



@Service
public class EmployeeService {
	@Autowired
	private DataSource dataSource;

	public Employee getEmployee(Integer id ) {

		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from emp where id="+id);
			
			while(resultSet.next()) {
				Employee employee = new Employee();
				employee.setId(resultSet.getInt("id"));
				employee.setName(resultSet.getString("name"));
				employee.setAdress(resultSet.getString("adress"));
				return employee;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	public Employee insertEmployee(Employee employee) {
		try {
			Connection connection=dataSource.getConnection();
			PreparedStatement pst=connection.prepareStatement("insert into emp values(?,?,?)");
			pst.setInt(1,employee.getId());
			pst.setString(2, employee.getName());
			pst.setString(3,employee.getAdress());
			if(pst.executeUpdate()>0) {
				return employee;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	public Employee updateEmployee(Employee employee2) {
		try {
			 Connection connection=dataSource.getConnection();
			 PreparedStatement pStatement=connection.prepareStatement("update emp set ename=?,eadd=? where id=?");
			 pStatement.setString(1, employee2.getName());
			 pStatement.setString(2, employee2.getAdress());
			 pStatement.setInt(3, employee2.getId());
			 if(pStatement.executeUpdate()>0) {
					return employee2;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
				return null;
	}

	public String deleteEmployee(Integer id) {
		try {
			 Connection connection=dataSource.getConnection();
			 PreparedStatement pStatement=connection.prepareStatement("delete from emp where id="+id);
		      
			 if(pStatement.executeUpdate()>0) {
					return "Delete" ;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		return null;
	}
	
}