package com.boot.hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boot.hello.employee.Employee;
import com.boot.hello.helper.EmployeeHelper;
import com.boot.hello.service.*;
import com.boot.hello.util.APIResponse;

@RestController
@RequestMapping("/api")
public class Employeecontroller {

	@Autowired

	private EmployeeHelper employeehelper;

	@GetMapping("/H")
	public String hello() {

		return "hiiii";
	}

	@GetMapping("/employee/{id}")
	public APIResponse<Employee> getEmployee(@PathVariable Integer id) {

		return employeehelper.getEmployee(id);
	}

	@PostMapping("/employee")
	public APIResponse<Employee> insertEmployee(@RequestBody Employee employee) {
		return employeehelper.insertEmployee(employee);
	}

	@PutMapping("/employee1")
	public APIResponse<Employee> updateEmployee(@RequestBody Employee employee) {
		return employeehelper.updateEmployee(employee);
	}

	@DeleteMapping("/employee1/{id}")
	public APIResponse<Employee> deleteEmployee(@PathVariable Integer id) {
		return employeehelper.deleteEmployee(id);
	}
}