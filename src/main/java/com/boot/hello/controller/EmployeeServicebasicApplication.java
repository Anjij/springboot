package com.boot.hello.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;

@SpringBootApplication(scanBasePackages ="com.boot.hello")
/*
 * @EnableAutoConfiguration(exclude={ DataSourceAutoConfiguration.class,
 * DataSourceTransactionManagerAutoConfiguration.class })
 */

public class EmployeeServicebasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeServicebasicApplication.class, args);
	}

}
